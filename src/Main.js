import React, { Component } from 'react';
import firebase from 'firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class Main extends Component {
  constructor(props){
    super(props)
    var config = {
      apiKey: "AIzaSyCxMLpGhYjtQEGSuBfXGkNdvkA0V2EPIPU",
      authDomain: "internalapps-216312.firebaseapp.com",
      databaseURL: "https://internalapps-216312.firebaseio.com",
      projectId: "internalapps-216312",
      storageBucket: "internalapps-216312.appspot.com"
    };
    firebase.initializeApp(config);
  }
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
        <Provider store={store}>
            <Router />
        </Provider>
    );
  }
}
export default Main;
