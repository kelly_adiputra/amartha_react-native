import React, {Component} from 'react';
import {Text, View, TouchableWithoutFeedback} from 'react-native';
import {Actions} from 'react-native-router-flux';
import {CardSection} from './CardSection';

class ListItem extends Component {

  employeeClick() {
    Actions.employeeUpdate({ employee: this.props.employee });
  }

  render(){
    const {description, title, date, bool} = this.props.employee;
    return (
      <TouchableWithoutFeedback onPress={this.employeeClick.bind(this)}>
        <View>
          <CardSection>
            <Text>
              Title : {title}{"\n"}
              Description : {description} {"\n"}
              date: {date}
            </Text>
            {bool == true && (<Text>
                        Marked
                      </Text>)
            }
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export default ListItem;
