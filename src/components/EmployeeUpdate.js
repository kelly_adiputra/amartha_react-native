import React, { Component } from 'react';
import { TextInput, Picker, Text } from 'react-native';
import { connect } from 'react-redux';
import { Button, Card, CardSection, Spinner } from '../components';
import { empChanged, empUpdate,empDelete } from '../actions';

class EmployeeUpdate extends Component {

  state ={title: '', description: '', date: '', bool: 0};
  componentWillMount(){
    const { title,
      description,
      date,
      bool } = this.props.employee
    
      this.props.empUpdate({ title, description, date, bool, uid: this.props.employee.uid });
      this.setState({title,description,date,bool});
  }


  render() {
    const { inputStyle } = styles;
    return (
      <Card>
        <CardSection>
          <Text>title : {this.state.title}</Text>
        </CardSection>

        <CardSection>
          <Text>description : {this.state.description}</Text>
        </CardSection>

        <CardSection>
          <Text>date : {this.state.date}</Text>
        </CardSection>

        
      </Card>
    );
  }
}

const styles = {

  inputStyle: {
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 10,
    lineHeight: 23,
    flex: 2
  }
};
const mapToStateProps = ({ empUpdateResponse }) => {
  const { loadingUpdate, loadingDelete } = empUpdateResponse;

    return { loadingUpdate, loadingDelete };
};

export default connect(mapToStateProps, { empChanged, empUpdate,empDelete })(EmployeeUpdate);
