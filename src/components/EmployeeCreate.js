import React, { Component } from 'react';
import { TextInput, Picker, Text,View, DatePickerIOS } from 'react-native';
import { connect } from 'react-redux';
import { Button, Card, CardSection, Spinner } from '../components';
import { empChanged, empCreated } from '../actions';

class EmployeeCreate extends Component {
  constructor(props){
    super(props)
    this.setDate = this.setDate.bind(this);
  }
  clickSave() {
    const { title,
      description,
      date } = this.props;

      this.props.empCreated({ title: title, description, date });
      console.log('Operation Success');
  }
  renderButton() {
    if (!this.props.loading) {
      return <Button onPress={this.clickSave.bind(this)}> Save </Button>;
    }
      return <Spinner size='small' />;
  }
  setDate(newDate) {
    console.log('cek', newDate)
    this.props.empChanged({ props: 'date', value: newDate });
  }
  render() {

    const { inputStyle } = styles;
    return (
      <Card>
        <CardSection>
          <TextInput
            placeholder="Title"
            style={inputStyle}
            value={this.props.title}
            onChangeText={title => this.props.empChanged({ props: 'title', value: title})}
          />
        </CardSection>

        <CardSection>
          <TextInput
            placeholder="Description"
            style={inputStyle}
            value={this.props.description}
            onChangeText={description => this.props.empChanged({ props: 'description', value: description })}
          />
        </CardSection>

        <View style={{flex:1,height:200,justifyContent: 'center'}}>
          <DatePickerIOS
            date={this.props.date || new Date()}
            onDateChange={this.setDate}
          />
          {/* <DatePickerIOS
            placeholder="Date"
            style={inputStyle}
            value={this.props.date}
            onValueChange={date => this.props.empChanged({ props: 'date', value: date })}
            // onChangeText={EmpNo => this.props.empChanged({ props: 'EmpNo', value: EmpNo })}
          /> */}
        </View>

        {/* <CardSection>
          <Text style={{ fontSize: 10, flex: 1, marginTop: 15 }}>Choose a Department : </Text>
          <Picker
            style={{ flex: 1 }}
            selectedValue={this.props.department}
            onValueChange={
              department => this.props.empChanged({ props: 'department',
                value: department })}
          >
            <Picker.Item label="Information Technologies" value="IT" />
            <Picker.Item label="Financial" value="FC" />
            <Picker.Item label="Logistics" value="LT" />
            <Picker.Item label="Human Resources" value="HR" />
          </Picker>
        </CardSection> */}

        <CardSection>
          {this.renderButton()}
        </CardSection>

      </Card>
    );
  }
}

const styles = {

  inputStyle: {
    color: '#000',
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 10,
    lineHeight: 23,
    flex: 2
  }
};
const mapToStateProps = ({ empListResponse }) => {
  const { title,
    description,
    date, loading } = empListResponse;

    return {
      title : title,
      description,
      date,
        loading
    };
};

export default connect(mapToStateProps, { empChanged, empCreated })(EmployeeCreate);
